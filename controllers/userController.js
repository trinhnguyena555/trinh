const bcrypt = require('bcrypt')
const { User } = require('../modal/modal')
const jwt = require('jsonwebtoken')
const authorControllers = require('./authorControllers')
const userController = {
    dangKyUser: async (req, res) => {
        try {
            const salt = await bcrypt.genSalt(10)
            const hashed = await bcrypt.hash(req.body.password, salt)
            const newUser = new User({
                userName: req.body.userName,
                userEmail: req.body.userEmail,
                password: hashed
            })
            const user = await newUser.save()
            res.status(200).json(user)
        } catch (error) {
            res.status(500).json(error)
        }
    },
    //login
    dangNhapUser: async (req, res) => {
        try {
            const user = await User.findOne({ userName: req.body.userName })
            if (!user) {
               return res.status(404).json("Lỗi username")
            }
            const valiPassword = await bcrypt.compare(
                req.body.password,
                user.password
            )

            if (!valiPassword) {
               return res.status(404).json("Lỗi password")
            }
            if (user && valiPassword) {
                const accessToken = jwt.sign({
                    id: user.id,
                    admin: user.admin
                },
                    "secretkey",
                    { expiresIn: '30s' }
                );
                const refreshToken = jwt.sign({
                    id: user.id,
                    admin: user.admin
                },
                    "secretke1",
                    { expiresIn: '360d' }
                );
                res.cookie("refreshToken",refreshToken,{
                    httpOnly:true,
                    secure:false,
                    path:"/",
                    sameSite:"strict"
                })
                const { password, ...outherd } = user._doc
                res.status(200).json({ ...outherd, accessToken })
            }
        } catch (error) {
            res.status(500).json(error)
        }
    },
    requestToken:async(req,res)=>{
        const refreshToken=req.cookies.refreshToken
        if (!refreshToken) {
            return res.status(401).json("you not authen")
        }
        jwt.verify(refreshToken,secretke1,(err,user)=>{
            if (err) {
                console.log(err);
            }
            const accessToken = jwt.sign({
                id: user.id,
                admin: user.admin
            },
                "secretkey",
                { expiresIn: '30s' }
            );
            const refreshToken = jwt.sign({
                id: user.id,
                admin: user.admin
            },
                "secretke1",
                { expiresIn: '360d' }
            );
            res.cookie("refreshToken",refreshToken,{
                httpOnly:true,
                secure:false,
                path:"/",
                sameSite:"strict"
            })
            
            res.status(200).json({ accessToken:refreshToken})
        })
    }
}
module.exports = userController