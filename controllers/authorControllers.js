const { Author, Book } = require('../modal/modal')

const authorControllers = {
    //ADD Author
    addAuthor: async (req, res) => {
       
        try {
            const newAuthor = new Author(req.body)
            
            const saveAuthor = await newAuthor.save()
            
            res.status(200).json(saveAuthor)
        } catch (error) {
            res.status(500).json(error)
        }

    },
    //get Author
    getAuthor: async (req, res) => {
        try {
            const author = await Author.find().populate('books')
            res.status(200).json(author)
        } catch (error) {
            res.status(500).json(error)

        }
    },
    //get an Author
    getAnAuthor: async (req, res) => {
        try {
            const author = await Author.findById(req.params.id).populate('books')
            res.status(200).json(author)
        } catch (error) {
            res.status(500).json(error)

        }
    },
     //update Author
    updateAuthor: async (req, res) => {
        try {
            const author = await Author.findById(req.params.id)
            await author.updateOne({$set:req.body})
            res.status(200).json("Update succfully")
        } catch (error) {
            res.status(500).json(error)

        }
    },
     //delete Author
     deleteAuthor: async (req, res) => {
        try {
            await Book.updateMany(
                {author:req.params.id},
                {author:null},

            )
             await Author.findByIdAndDelete(req.params.id)
            
            res.status(200).json("Update succfully")
        } catch (error) {
            res.status(500).json(error)

            

        }
    }
    
}
var avav;
module.exports = authorControllers