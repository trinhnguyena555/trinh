const { User } = require("../modal/modal")

const usersController={
    getUsers:async (req,res)=>{
        try {
            const users=await User.find()
            res.status(200).json(users)
        } catch (error) {
            res.status(500).json(error)
        }
    },
    deleteUsers:async (req,res)=>{
        try {
            const users=await User.findById(req.params.id)
            res.status(200).json("Delete successfully")
        } catch (error) {
            res.status(500).json(error)
        }
    }
}
module.exports=usersController