const { Author, Book } = require('../modal/modal')

const bookController = {
    //ADD Author
    addBook: async (req, res) => {
        try {
            const newBook = new Book(req.body)
            const saveBook = await newBook.save()
            if (req.body.author) {
                const author=Author.findById(req.body.author)
                await author.updateOne({$push:{books:saveBook._id}})
            }
            res.status(200).json(saveBook)
        } catch (error) {
            res.status(500).json(error)
        }

    },
    //get Book
    getBook: async (req, res) => {
        try {
            const book = await Book.find()
            res.status(200).json(book)
        } catch (error) {
            res.status(500).json(error)

        }
    },
     //get an Book
     getAnBook: async (req, res) => {
        try {
            const book = await Book.findById(req.params.id).populate('author')
            res.status(200).json(book)
        } catch (error) {
            res.status(500).json(error)

        }
    },
     //update an Book
    updateBook: async (req, res) => {
        try {
            const book = await Book.findById(req.params.id)
            await book.updateOne({$set:req.body})
            res.status(200).json("Update succfuly")
        } catch (error) {
            res.status(500).json(error)

        }
    },
     //delete an Book
    deleteBook: async (req, res) => {
        try {
            await Author.updateMany(
                {books:req.params.id},
                {$pull:{books:req.params.id}},

            )
            await Book.findByIdAndDelete(req.params.id)           
            res.status(200).json("delete succfuly")
        } catch (error) {
            res.status(500).json(error)

        }
    }
}
module.exports = bookController