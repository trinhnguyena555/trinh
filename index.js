const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const db=require('./configDatabase/db/connext')
const authorRouter=require('./routes/author')
const bookRouter=require('./routes/book')
const cookieParser=require('cookie-parser')
const cors=require('cors')
const userRouter=require('./routes/user')
const usersRouter=require('./routes/users')
const app = express()

app.use(cookieParser())
app.use(express.json())
app.use(bodyParser.json({limit:"50mb"}))
app.use(cors())
app.use(morgan("common"))

//Connect Database
db.connect()
for (let index = 0; index < [].length; index++) {
  const element = array[index];
  
}
//router Author
app.use("/v1/author",authorRouter)

// router Book
app.use('/v1/book',bookRouter)
// router User
app.use('/v1/user',userRouter)
// router User
app.use('/v1/users',usersRouter)


app.listen(3000, () => {
    console.log(`Bạn đang chạy cổng 8000`)
  })