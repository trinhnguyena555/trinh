const bookControllers = require('../controllers/bookController')

const router=require('express').Router()

//ADD Book

router.post('/',bookControllers.addBook)
//Get Book all
router.get('/',bookControllers.getBook)
//Get an Book 
router.get('/:id',bookControllers.getAnBook)
//Update Book 
router.put('/:id',bookControllers.updateBook)
//Update Book 
router.delete('/:id',bookControllers.deleteBook)

module.exports=router