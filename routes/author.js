const authorControllers = require('../controllers/authorControllers')

const router=require('express').Router()

//ADD Author

router.post('/',authorControllers.addAuthor)
//Get all author
router.get('/',authorControllers.getAuthor)
//Get an athor
router.get('/:id',authorControllers.getAnAuthor)
//update athor
router.put('/:id',authorControllers.updateAuthor)
//update athor
router.delete('/:id',authorControllers.deleteAuthor)

module.exports=router