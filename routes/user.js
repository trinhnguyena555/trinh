const userController = require('../controllers/userController')

const router=require('express').Router()
router.post('/dangky',userController.dangKyUser)
router.post('/dangnhap',userController.dangNhapUser)
router.post('/refresh ',userController.requestToken)

module.exports=router