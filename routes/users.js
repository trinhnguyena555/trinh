const middlewareController = require('../controllers/middlewareController')
const usersController = require('../controllers/usersController')

const router=require('express').Router()
router.get('/getuser',middlewareController.verifyToken,usersController.getUsers)
router.delete('/:id',middlewareController.verifyTokenAndAdmin,usersController.deleteUsers)
router.post('/refresh',middlewareController.requestRefreshToken)

module.exports=router 