const mongoose = require('mongoose');
async function connect() {
    try {
        await mongoose.connect('mongodb://localhost:27017/homeBook')
        console.log("Connect successfuly");
    } catch (error) {
        console.log("Connect error");
    }    
}
module.exports={connect}