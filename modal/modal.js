const mongoose=require('mongoose')


const userSchema=new mongoose.Schema({
    userName:{type:String,required:true,minlength:6,maxlength:50,require:true},
    userEmail:{type:String,required:true,minlength:6,maxlength:50,require:true},
    password:{type:String,required:true},
    admin:{type:Boolean,default:false}
},{
    timestamps:true
})

const authorSchema=new mongoose.Schema({
    name:{type:String,required:true},
    year:{type:Number},
    books:[{type:mongoose.Schema.Types.ObjectId,ref:"Book"}],
},{
    timestamps:true
})

const bookSchema=new mongoose.Schema({
    name:{type:String,required:true},
    publishedDate:{type:String,required:true},
    genres:{type:[String],required:true},
    author:{type:mongoose.Schema.Types.ObjectId,ref:"Author"},
},{
    timestamps:true
})
let User=mongoose.model("User",userSchema)
let Book=mongoose.model("Book",bookSchema)
let Author=mongoose.model("Author",authorSchema)


module.exports={User,Book,Author}